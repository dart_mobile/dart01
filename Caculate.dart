import 'dart:io';

void main(List<String> args) {
  print("MENU");
  print("Select the choice you want to perform :");
  print("1.ADD");
  print("2.SUBTRACT");
  print("3.MULTIPLY");
  print("4.DIVIDE");
  print("5.EXIT");

  print("choice you want to enter :");
  int? select_method = int.parse(stdin.readLineSync()!);

  switch (select_method) {
    case 1:
      {
        print("Enter the value for X");
        int? a_Value = int.parse(stdin.readLineSync()!);
        print("Enter the value for Y");
        int? b_Value = int.parse(stdin.readLineSync()!);

        int? answer = a_Value + b_Value;
        print("Ans of the two numbers is :");
        print(answer);
        break;
      }
    case 2:
      {
        print("Enter the value for X");
        int? a_Value = int.parse(stdin.readLineSync()!);
        print("Enter the value for Y");
        int? b_Value = int.parse(stdin.readLineSync()!);

        int? answer = a_Value - b_Value;
        print("Ans of the two numbers is :");
        print(answer);
        break;
      }
    case 3:
      {
        print("Enter the value for X");
        int? a_Value = int.parse(stdin.readLineSync()!);
        print("Enter the value for Y");
        int? b_Value = int.parse(stdin.readLineSync()!);

        int? answer = a_Value * b_Value;
        print("Ans of the two numbers is :");
        print(answer);
        break;
      }
    case 4:
      {
        print("Enter the value for X");
        int? a_Value = int.parse(stdin.readLineSync()!);
        print("Enter the value for Y");
        int? b_Value = int.parse(stdin.readLineSync()!);

        double? answer = a_Value / b_Value;
        print("Ans of the two numbers is :");
        print(answer);
        break;
      }
    case 5:
      {
        break;
      }
  }
}
