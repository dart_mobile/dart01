void main(List<String> args) {
  const String s =
      'Dart is a client-optimized language for developing fast apps on any platform. Its goal is to offer the most productive programming language for multi-platform development, paired with a flexible execution runtime platform for app frameworks.Languages are defined by their technical envelope—the choices made during development that shape the capabilities and strengths of a language. Dart is designed for a technical envelope that is particularly suited to client development, prioritizing both development (sub-second stateful hot reload) and high-quality production experiences across a wide variety of compilation targets (web, mobile, and desktop).Dart also forms the foundation of Flutter. Dart provides the language and runtimes that power Flutter apps, but Dart also supports many core developer tasks like formatting, analyzing, and testing code.';
  var chagetoLow = s.toLowerCase();
  final List l = chagetoLow.split(' ');
  int count = l.length;
  print(count);
  List wordsList = [];
  List count_word = [];
  for (int i = 0; i < l.length; i++) {
    var duplicate = false;
    for (int j = 0; j < wordsList.length; j++) {
      if (l[i] == wordsList[j]) {
        duplicate = true;
        count_word[j] = count_word[j] + 1;
        break;
      }
    }
    if (duplicate == false) {
      wordsList.add(l[i]);
      count_word.add(1);
    }
  }
  for (var i = 0; i < wordsList.length; i++) {
    print("${wordsList[i]} ${count_word[i]}");
  }
}
