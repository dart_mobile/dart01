void main() {
  var list1 = [1, 2, 3];
  var list2 = [1];
  print(' \nList 1 : ');
  print(list1);
  print(' \nList 2 : ');
  print(list2);
  list1.add(4);
  print(' \nList 1 after adding 4 : ');
  print(list1);
  list1.addAll([5, 6, 7, 8]);
  print(' \nList 1 after adding 5, 6, 7, 8 : ');
  print(list1);
  list2.insert(1, 11);
  list2.insert(2, 12);
  print(' \nList 2 after adding 11 and 12 at 1st and 2nd index : ');
  print(list2);
  list2.insertAll(2, [13, 14, 15, 16]);
  print(' \nList 2 after adding 13, 14, 15, 16 at index 2 : ');
  print(list2);
  list2.replaceRange(2, 4, [21, 22, 23]);
  print(
      ' \nList 2 after replacing elements between 2nd and 4th index  with 21, 22, 23 :');
  print(list2);
  list2.remove(21);
  print(' \nList 2 after removing 21 value : ');
  print(list2);
  list1.removeAt(2);
  print(' \nList 1 after removing element at the 2nd index : ');
  print(list1);
  list2.removeLast();
  print(' \nList 2 after removing the last element : ');
  print(list2);
  list1.removeRange(1, 3);
  print(
      ' \nList 1 after adding removing the elements between 1st and 3rd index : ');
  print(list1);
}
