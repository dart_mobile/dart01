import 'dart:io';

bool func1(dynamic prime) {
  if (prime < 1) {
    return false;
  }
  for (int i = 2; i < prime / 2; i++) {
    if ((prime % i) == 0) return false;
  }
  return true;
}

void main(List<String> args) {
  print(" input your prime number : ");
  int? prime = int.parse(stdin.readLineSync()!);
  print(func1(prime));
}
