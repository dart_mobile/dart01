void func1(var a, [var b]) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
}

void main() {
  // Calling the function with optional parameter
  print(" Calling the function with optional parameters : ");
  func1(01,[2,3,4]);
}
